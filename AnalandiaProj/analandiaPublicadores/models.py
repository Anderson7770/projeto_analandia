from django.db import models

class PublicadorAnalandia(models.Model):
    #ano_batismo = models.CharField(max_length=10)
    nome = models.CharField(max_length=100)
    idade = models.CharField(max_length=20)
    horas_campo = models.IntegerField()
    privilegios = models.CharField(max_length=100)

    def __str__(self):
        return self.nome
