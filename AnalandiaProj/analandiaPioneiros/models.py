from django.db import models


class PioneirosAnalandia(models.Model):
    #batismo = models.CharField(max_length=10)
    nome = models.CharField(max_length=100)
    idade = models.IntegerField()
    horas_campo = models.IntegerField()
    privilegio = models.CharField(max_length=100)
    
    def __str__(self):
        return self.nome