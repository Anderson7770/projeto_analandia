from django.apps import AppConfig


class AnalandiapioneirosConfig(AppConfig):
    name = 'analandiaPioneiros'
