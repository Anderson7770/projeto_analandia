from django.db import models


class ServosAnalandia(models.Model):
    #batismo = models.CharField(max_length=10)
    nome = models.CharField(max_length=49)
    idade = models.CharField(max_length=19)
    horas_campo = models.IntegerField()
    provilegios = models.CharField(max_length=99)


    def __str__(self):
        return self.nome
