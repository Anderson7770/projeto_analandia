from django.db import models

# Aqui sao feitos os relacionamentos de modelos do django
# Aqui é onde os anciaos controlam os outros da congregacao pelo
# sistema
from AnalandiaProj.analandiaServo.models import ServosAnalandia
from AnalandiaProj.analandiaPioneiros.models import PioneirosAnalandia
from AnalandiaProj.analandiaPublicadores.models import PublicadorAnalandia

class AnciaosAnalandia(models.Model):
    #batismo = models.DateTimeField(null=True, blank=True)
    nome = models.CharField(max_length=50)
    idade = models.CharField(max_length=20)
    horas_campo = models.IntegerField()
    privilegios = models.CharField(max_length=100)
    
    # Relacionamentos de modelos com django
    servo = models.ManyToManyField(ServosAnalandia)
    pioneiro = models.ManyToManyField(PioneirosAnalandia)
    publicador = models.ManyToManyField(PublicadorAnalandia)

    def __str__(self):
        return self.nome